package com.directorial1.theguardiannews

import android.app.Application
import com.directorial1.theguardiannews.di.appModule
import com.directorial1.theguardiannews.di.repoModule
import com.directorial1.theguardiannews.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(listOf(appModule, repoModule, viewModelModule))
        }
    }
}