package com.directorial1.theguardiannews.viewmodel.repo

import androidx.lifecycle.LiveData
import com.directorial1.theguardiannews.database.entity.MainModel
import com.directorial1.theguardiannews.data.NewsModel
import retrofit2.Response

interface NewsRepository {

    suspend fun getAllData() : Response<NewsModel>

    suspend fun saveNews(mainModel: MainModel)

    fun getSavedNews(): LiveData<List<MainModel>>
}