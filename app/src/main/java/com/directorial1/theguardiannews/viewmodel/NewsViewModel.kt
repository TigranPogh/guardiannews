package com.directorial1.theguardiannews.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.directorial1.theguardiannews.database.entity.MainModel
import com.directorial1.theguardiannews.data.ResultsItem
import com.directorial1.theguardiannews.viewmodel.repo.NewsRepository
import kotlinx.coroutines.launch

class NewsViewModel(private val repository : NewsRepository) : ViewModel(){

    private var newsMutableLiveData = MutableLiveData<List<ResultsItem>>()
    var item = MainModel("", "", "")
    var type = ""
    var favoritesList : MutableList<MainModel> = emptyList<MainModel>().toMutableList()

    fun getNews() = viewModelScope.launch {
        try {
            val response = repository.getAllData()
            if(response.isSuccessful && response.body() != null) {
                newsMutableLiveData.value = response.body()!!.response!!.results as List<ResultsItem>?
            }
        } catch (ex : Exception) {

        }
    }

    fun getNewsLiveData() : LiveData<List<ResultsItem>> = newsMutableLiveData

    fun saveNews(mainModel: MainModel) = viewModelScope.launch {
        repository.saveNews(mainModel)
    }

    fun getSavedNewsLiveData() : LiveData<List<MainModel>> = repository.getSavedNews()
}