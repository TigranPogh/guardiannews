package com.directorial1.theguardiannews.viewmodel.repo

import androidx.lifecycle.LiveData
import com.directorial1.theguardiannews.API_KEY
import com.directorial1.theguardiannews.API_THUMBS
import com.directorial1.theguardiannews.PAGE_SIZE
import com.directorial1.theguardiannews.database.entity.MainModel
import com.directorial1.theguardiannews.database.dao.NewsDAO
import com.directorial1.theguardiannews.network.ApiService
import retrofit2.Response

class NewsRepositoryImpl(
    private val apiService: ApiService,
    private val newsDAO: NewsDAO
) : NewsRepository {

    override suspend fun getAllData(): Response<com.directorial1.theguardiannews.data.NewsModel> {
        return apiService.getData(API_KEY, API_THUMBS, PAGE_SIZE)
    }

    override suspend fun saveNews(mainModel: MainModel) {
        newsDAO.insert(mainModel)
    }

    override fun getSavedNews(): LiveData<List<MainModel>> = newsDAO.all
}