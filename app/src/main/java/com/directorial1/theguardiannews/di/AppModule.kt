package com.directorial1.theguardiannews.di

import android.app.Application
import androidx.room.Room
import com.directorial1.theguardiannews.BASE_URL
import com.directorial1.theguardiannews.BuildConfig
import com.directorial1.theguardiannews.database.AppDatabase
import com.directorial1.theguardiannews.database.dao.NewsDAO
import com.directorial1.theguardiannews.network.ApiService
import com.directorial1.theguardiannews.viewmodel.NewsViewModel
import com.directorial1.theguardiannews.viewmodel.repo.NewsRepository
import com.directorial1.theguardiannews.viewmodel.repo.NewsRepositoryImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
    OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .build()
} else OkHttpClient
    .Builder()
    .build()

private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
    Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

private fun provideApiService(retrofit: Retrofit): ApiService =
    retrofit.create(ApiService::class.java)

private fun provideDatabase(application: Application) : AppDatabase {
    return  Room.databaseBuilder(application, AppDatabase::class.java, "database").build()
}

private fun provideDao(appDatabase: AppDatabase) : NewsDAO? {
    return appDatabase.newsDAO()
}

val appModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }

    single { provideDatabase(androidApplication())}
    single { provideDao(get())}
}

val repoModule = module {
    single<NewsRepository> {
        return@single NewsRepositoryImpl(get(), get())
    }
}

val viewModelModule = module {
    viewModel {
        NewsViewModel(get())
    }
}
