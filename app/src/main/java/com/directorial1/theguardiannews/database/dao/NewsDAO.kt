package com.directorial1.theguardiannews.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.directorial1.theguardiannews.database.entity.MainModel

@Dao
interface NewsDAO {
    @get:Query("SELECT * FROM dao_model")
    val all: LiveData<List<MainModel>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(mainModel: MainModel?)
}