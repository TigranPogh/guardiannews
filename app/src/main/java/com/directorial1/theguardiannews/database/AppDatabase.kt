package com.directorial1.theguardiannews.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.directorial1.theguardiannews.database.entity.MainModel
import com.directorial1.theguardiannews.database.dao.NewsDAO

@Database(entities = [MainModel::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun newsDAO(): NewsDAO?
}