package com.directorial1.theguardiannews.network

import com.directorial1.theguardiannews.QUERY_API_KEY
import com.directorial1.theguardiannews.QUERY_PAGE_SIZE
import com.directorial1.theguardiannews.QUERY_SHOW_FIELDS
import com.directorial1.theguardiannews.SEARCH_END_POINT
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET(SEARCH_END_POINT)
    suspend fun getData(
        @Query(QUERY_API_KEY )apiKey : String,
        @Query(QUERY_SHOW_FIELDS)showThumbs : String,
        @Query(QUERY_PAGE_SIZE) pageSize : Int) : Response<com.directorial1.theguardiannews.data.NewsModel>
}