package com.directorial1.theguardiannews.ui.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.directorial1.theguardiannews.R
import com.directorial1.theguardiannews.SAVED_NEWS_SPAN_COUNT
import com.directorial1.theguardiannews.databinding.FragmentNewsBinding
import com.directorial1.theguardiannews.ui.adapters.FavoritesAdapter
import com.directorial1.theguardiannews.ui.adapters.NewsAdapter
import com.directorial1.theguardiannews.ui.adapters.SavedNewsAdapter
import com.directorial1.theguardiannews.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.fragment_news.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class NewsFragment : Fragment() {

    private val viewModel: NewsViewModel by sharedViewModel()
    private lateinit var binding: FragmentNewsBinding
    private lateinit var newsAdapter: NewsAdapter
    private lateinit var savedNewsAdapter: SavedNewsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addFavorites()
        showViews()
    }

    private fun initObservers() {
        viewModel.getNewsLiveData().observe(requireActivity(), Observer {items ->
            newsAdapter.setItems(items)
            binding.progressBarId.visibility = GONE
        })
    }

    private fun initSavedNewsObserver() {
        viewModel.getSavedNewsLiveData().observe(requireActivity(), Observer {savedNewsList ->
            savedNewsAdapter.setItems(savedNewsList)
            binding.progressBarId.visibility = GONE
        })
    }

    @Suppress("DEPRECATION")
    private fun hasNetworkAvailable(context: Context): Boolean {
        val service = Context.CONNECTIVITY_SERVICE
        val manager = context.getSystemService(service) as ConnectivityManager?
        val network = manager?.activeNetworkInfo
        return (network != null)
    }

    private fun showViews() {
        if (hasNetworkAvailable(requireActivity())) {
            viewModel.getNews()
            initNewsAdapter()
            initObservers()
        }else{
            initSavedNewsAdapter()
            initSavedNewsObserver()
        }
    }

    private fun initNewsAdapter() {
            newsAdapter = NewsAdapter{ mainModel, type ->
                viewModel.item = mainModel
                viewModel.type = type
                findNavController(requireView()).navigate(R.id.action_newsFragment_to_detailsFragment)
        }
        newsRecyclerView.adapter = newsAdapter
        newsRecyclerView.layoutManager = LinearLayoutManager(requireActivity())
    }

    private fun initSavedNewsAdapter() {
        savedNewsAdapter = SavedNewsAdapter { mainModel, type ->
                viewModel.item = mainModel
                viewModel.type = type
                findNavController(requireView()).navigate(R.id.action_newsFragment_to_detailsFragment)
        }
        newsRecyclerView.adapter = savedNewsAdapter
        newsRecyclerView.layoutManager = GridLayoutManager(requireActivity(), SAVED_NEWS_SPAN_COUNT)
    }

    private fun addFavorites(){
        val favoritesList = viewModel.favoritesList
        val adapter = FavoritesAdapter{ mainModel, type ->
            viewModel.item = mainModel
            viewModel.type = type
            findNavController(requireView()).navigate(R.id.action_newsFragment_to_detailsFragment)
        }
        adapter.setItems(favoritesList)
        binding.hrRecyclerView.adapter = adapter
    }
}