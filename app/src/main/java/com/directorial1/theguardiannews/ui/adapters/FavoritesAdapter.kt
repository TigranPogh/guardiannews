package com.directorial1.theguardiannews.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.directorial1.theguardiannews.NEWS_TYPE_FAVORITE
import com.directorial1.theguardiannews.database.entity.MainModel
import com.directorial1.theguardiannews.databinding.FavoritesItemBinding

class FavoritesAdapter(private val itemClick: ((MainModel, String) -> Unit)? = null) : RecyclerView.Adapter<FavoritesAdapter.FavoritesHolder>() {

    private var list : List<MainModel> = emptyList()
    private lateinit var binding : FavoritesItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesHolder {
        binding = FavoritesItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return FavoritesHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: FavoritesHolder, position: Int) {
        holder.bind(list[position], itemClick ?: { _, _ ->  print("Clicked") })
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(list: List<MainModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    class FavoritesHolder(private val binding: FavoritesItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(mainModel: MainModel, itemClick: ((MainModel, String) -> Unit)) {
            binding.item = mainModel
            itemView.setOnClickListener { itemClick(mainModel, NEWS_TYPE_FAVORITE) }
        }
    }
}