package com.directorial1.theguardiannews.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.directorial1.theguardiannews.NEWS_TYPE_FAVORITE
import com.directorial1.theguardiannews.NEWS_TYPE_SAVED
import com.directorial1.theguardiannews.database.entity.MainModel
import com.directorial1.theguardiannews.databinding.FragmentDetailsBinding
import com.directorial1.theguardiannews.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DetailsFragment : Fragment() {

    private val viewModel: NewsViewModel by sharedViewModel()
    private lateinit var mainModel: MainModel
    private lateinit var binding: FragmentDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        binding.viewmodel = viewModel
        binding.fragment = this
        mainModel = viewModel.item
        return binding.root
    }

    @ObsoleteCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showIcons()
    }

    private fun showIcons(){
        when(viewModel.type) {
            NEWS_TYPE_FAVORITE -> imgFavorite.visibility = GONE
            NEWS_TYPE_SAVED -> imgSave.visibility = GONE
        }
    }

     fun goBack() {
        requireActivity().onBackPressed()
    }

     fun onSaveClicked() {
         viewModel.saveNews(mainModel)
        Toast.makeText(requireActivity(), "Saved for offline reading", Toast.LENGTH_SHORT).show()
    }

     fun onFavoriteClick() {
        if (!viewModel.favoritesList.contains(mainModel)) {
            viewModel.favoritesList.add(mainModel)
            Toast.makeText(requireActivity(), "Added to favorites list", Toast.LENGTH_SHORT).show()
        }
    }
}