package com.directorial1.theguardiannews.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.directorial1.theguardiannews.NEWS_TYPE_SAVED
import com.directorial1.theguardiannews.database.entity.MainModel
import com.directorial1.theguardiannews.databinding.NewsItemBinding

class SavedNewsAdapter(private val itemClick: ((MainModel, String) -> Unit)? = null) : RecyclerView.Adapter<SavedNewsAdapter.SavedNewsHolder>() {

    private var list: List<MainModel> = emptyList()
    private lateinit var binding : NewsItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedNewsHolder {
        binding = NewsItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return SavedNewsHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: SavedNewsHolder, position: Int) {
        holder.bind(list[position], itemClick ?: { _, _ ->  print("Clicked") })
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(list: List<MainModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    class SavedNewsHolder(private val binding: NewsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(mainModel: MainModel, itemClick: ((MainModel, String) -> Unit)) {
            binding.item = mainModel
            itemView.setOnClickListener { itemClick(mainModel, NEWS_TYPE_SAVED) }
        }
    }
}