package com.directorial1.theguardiannews.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.directorial1.theguardiannews.database.entity.MainModel
import com.directorial1.theguardiannews.data.ResultsItem
import com.directorial1.theguardiannews.databinding.NewsItemBinding

class NewsAdapter(private val itemClick: ((MainModel, String) -> Unit)? = null) : RecyclerView.Adapter<NewsAdapter.NewsHolder>() {

    private var list: List<ResultsItem> = emptyList()
    private lateinit var binding : NewsItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        binding = NewsItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return NewsHolder(
           binding
        )
    }

    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        holder.bind(list[position], itemClick ?: { _, _ ->  print("Clicked") })
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(list: List<ResultsItem>) {
        this.list = list
        notifyDataSetChanged()
    }

    class NewsHolder(private val binding: NewsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(newsModel: ResultsItem, itemClick: ((MainModel, String) -> Unit)) {
            val mainModel = MainModel(newsModel.sectionName!!, newsModel.webTitle!!, newsModel.fields?.thumbnail)
            binding.item = mainModel
            itemView.setOnClickListener { itemClick(mainModel,"") }
        }
    }
}
