package com.directorial1.theguardiannews

const val BASE_URL = "https://content.guardianapis.com/"
const val SEARCH_END_POINT = "search"
const val API_KEY = "ca817211-bbd2-462c-9e7a-7b6309d20184"
const val API_THUMBS = "thumbnail"
const val PAGE_SIZE = 100
const val SPLASH_DELAY: Long = 2000
const val NEWS_TYPE_FAVORITE = "favorite"
const val NEWS_TYPE_SAVED = "saved"
const val QUERY_API_KEY = "api-key"
const val QUERY_SHOW_FIELDS = "show-fields"
const val QUERY_PAGE_SIZE = "page-size"
const val SAVED_NEWS_SPAN_COUNT = 2