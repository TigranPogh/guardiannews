package com.directorial1.theguardiannews

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.api.load
import coil.transform.CircleCropTransformation
import com.squareup.picasso.Picasso

@BindingAdapter("eventImage")
fun setImage(imageView: ImageView, url: String?) {
    Picasso.get().load(url).into(imageView)
}

@BindingAdapter("circleImage")
fun setCircleImage(imageView: ImageView, url: String?) {
    imageView.load(url){
        transformations(CircleCropTransformation())
    }
}

