package com.directorial1.theguardiannews.data

import com.google.gson.annotations.SerializedName

data class NewsModel(

	@field:SerializedName("response")
	val response: Response? = null
)
