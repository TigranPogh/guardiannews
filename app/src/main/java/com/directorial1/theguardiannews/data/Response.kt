package com.directorial1.theguardiannews.data

import com.google.gson.annotations.SerializedName

data class Response(

    @field:SerializedName("userTier")
    val userTier: String? = null,

    @field:SerializedName("total")
    val total: Int? = null,

    @field:SerializedName("startIndex")
    val startIndex: Int? = null,

    @field:SerializedName("pages")
    val pages: Int? = null,

    @field:SerializedName("pageSize")
    val pageSize: Int? = null,

    @field:SerializedName("orderBy")
    val orderBy: String? = null,

    @field:SerializedName("currentPage")
    val currentPage: Int? = null,

    @field:SerializedName("results")
    val results: List<ResultsItem?>? = null,

    @field:SerializedName("status")
    val status: String? = null
)