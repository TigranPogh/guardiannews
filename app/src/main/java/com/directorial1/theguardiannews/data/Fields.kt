package com.directorial1.theguardiannews.data

import com.google.gson.annotations.SerializedName

data class Fields(

    @field:SerializedName("thumbnail")
    val thumbnail: String? = null
)