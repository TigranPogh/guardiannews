package com.directorial1.theguardiannews.data

import com.google.gson.annotations.SerializedName

data class ResultsItem(

    @field:SerializedName("sectionName")
    val sectionName: String? = null,

    @field:SerializedName("pillarName")
    val pillarName: String? = null,

    @field:SerializedName("webPublicationDate")
    val webPublicationDate: String? = null,

    @field:SerializedName("apiUrl")
    val apiUrl: String? = null,

    @field:SerializedName("webUrl")
    val webUrl: String? = null,

    @field:SerializedName("isHosted")
    val isHosted: Boolean? = null,

    @field:SerializedName("pillarId")
    val pillarId: String? = null,

    @field:SerializedName("webTitle")
    val webTitle: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("sectionId")
    val sectionId: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("fields")
    val fields: Fields? = null
)